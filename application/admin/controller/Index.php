<?php

namespace app\admin\controller;

use think\Controller;
use Request;

class Index extends Controller
{
    /**
     * 控制器的初始化方法
     */
    protected function initialize()
    {

    }
    public function index()
    {
        return '这是后台首页';
    }
}
