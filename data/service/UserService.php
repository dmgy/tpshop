<?php

namespace data\service;

use data\model\User;
use Session,SC,OnlyLogin;


class UserService
{
      /**
       * 定义所有的登入方式
       */
      private $loginWay = [
          'user_name', //
          'user_email',//
          'user_tel',  //
          'uid'        //
      ];
      public function __construct()
      {

      }

      public function login($username, $password)
      {
          // 一般做登入 先判断用户名是否存在，如存在我们就判断用户的密码是否正确
          foreach ($this->loginWay as $key => $value) {
              $user = User::where($value, $username)->find();
              if ($user) { // 如果存在就证明是有这个用户
                  break;
              }
          }

          if (!$user) {
             // 用户名是否存在
             // return ['code' => false, 'msg' => 'ヾヾ( ˘ ³˘人)ヾ用户名不存在'];
             // return 1;
             return ERROR_NO_USER;
          }

          if ($user->user_status != 1) {
             // 用户是否被封
             // return [ 'code' => false,'msg' => '用户名被封'];
             // return 1;
             return ERROR_USER_START;
          }

          // if (!password_verify($password ,$user->user_password)) {
          //     // 用户密码是否错误
          //     // return ['code' => false,'msg' => '用户名密码错误'];
          //     // return 1;
          //     return ERROR_PASSWORD;
          // }
          // 用户登入成功
          // return  ['code' => true,'msg' => '登入成功'];
          // return 1;
          // 这是用户登入之后的操作
          // 除了用户的基本信息的缓存
          $this->initLogin($user);
          OnlyLogin::onlyRecord($user->uid);
          return SUCCESS;
      }

      /**
       * 登入初始化
       */
      public function initLogin($user)
      {

          // // 用户登入记录

          SC::setLogin(true); // 根据自己安全需求
          // if ($user->is_system == 1) { // 这么写的原因
          //
          // } else {
          //
          // }
          SC::setUserInfo([
              'uid'       => $user->uid,
              'user_name' => $user->user_name,
              'is_system' => $user->is_system,
              'nick_name' => $user->nick_name,
              //...
              // 'role_id'   => $user->role_id
          ]);

          // 用户登入之后信息记入
          $data = [
              'current_login_ip'   => request()->ip(),
              'current_login_time' => date('Y-m-d H:i:s', time()),
              'last_login_time'    => $user->last_login_time
              // ...
              // 不同项目有不同的操作
          ];
          User::where('uid', $user->uid)->update($data);

          //
          // Session::set('USER_INFO_SESSION', $data);
          // Session::set('USER_Role_SESSION', $user->role_id);
          // Session::set('USER_IS_SYSTEM', $user->is_system);

          // 比如用户登入成功之后发送短信提醒用户那么你可以把这个操作写在钩子
          // 注意：发送短信的动作写在钩子中
      }
}
