<?php

namespace data\model\user;

use think\Model;

class UserRole extends Model
{

      /**
       * 数据表主键 复合主键使用数组定义
       * @var string|array
       */
      protected $pk = 'role_id';
}
