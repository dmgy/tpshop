<?php

namespace data\behavior;

use Config;
use think\Facade; // 是用来facade注册
use think\Loader; // 是用来别名注册

class LoadBehavior
{
    public function run()
    {
        // 如果不知道 请看think\base.php对于核心类库文件的代理和别名注册
        // facade注册
        Facade::bind(Config::get('facade.facade'));
        // 别名注册
        Loader::addClassAlias(Config::get('facade.alias'));
    }
}
